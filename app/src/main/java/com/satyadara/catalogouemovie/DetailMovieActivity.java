package com.satyadara.catalogouemovie;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.satyadara.catalogouemovie.model.Movie;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DetailMovieActivity extends AppCompatActivity {
    public static final String MOVIE_ITEM = "movie";
    private Movie movie;
    private ImageView ivPoster;
    private TextView tvTitle;
    private TextView tvOverview;
    private TextView tvReleasedDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_movie);
        getBundle();

        ivPoster = (ImageView) findViewById(R.id.image_poster_detail);
        tvTitle = (TextView) findViewById(R.id.title_detail);
        tvOverview = (TextView) findViewById(R.id.overview_detail);
        tvReleasedDate = (TextView) findViewById(R.id.release_detail);

        setValuesMovieItem();
    }

    private void getBundle() {
        Bundle bundle = getIntent().getExtras();
        movie = bundle.getParcelable(MOVIE_ITEM);
        Log.d("Movie Data ", movie.toString());
    }

    private void setValuesMovieItem() {
        Picasso.get()
                .load("http://image.tmdb.org/t/p/w185/" + movie.getPosterPath())
                .into(ivPoster);
        tvTitle.setText(movie.getTitle() + "(" + movie.getOriginalLanguage() + ")");
        try {
            SimpleDateFormat formatToString = new SimpleDateFormat("E, MMM dd, yyyy");
            SimpleDateFormat formatToDate = new SimpleDateFormat("yyyy-mm-dd");
            Date date = formatToDate.parse(movie.getReleaseDate());
            tvReleasedDate.setText(formatToString.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        tvOverview.setText(movie.getOverview());
    }
}
